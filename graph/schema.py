from django.conf import settings

import graphene

import boto3

from botocore.exceptions import ClientError


class BucketMessage(graphene.ObjectType):
    """
        I don't understand why it is required.
        The code does not work without it.
    """
    message = graphene.String()


class DeleteBucket(graphene.Mutation):
    """
        GraphiQL Command to to create a new bucket on s3

        {
            deleteBucket(bucketName: "dragon333")
        }

    """
    class Arguments:
        bucket_name = graphene.String()

    message = graphene.String()

    def mutate(self, info, bucket_name):
        message = ""

        try:
            s3_client = boto3.client('s3')
            s3_client.delete_bucket(Bucket=bucket_name)

            message = 'Bucket {0} deleted successfully.'.format(bucket_name)

        except ClientError as e:
            message = e.response["Error"]["Message"]
        except Exception as e:
            message = str(e)

        return BucketMessage(message)


class CreateBucket(graphene.Mutation):
    """

        GraphiQL Command to to create a new bucket on s3

        {
            createBucket(bucketName: "dragon333") {
                message
            }
        }

        OR

        {
            createBucket(bucketName: "dragon333")
        }


        region is optional

        {
            createBucket(bucketName: "dragon333", region: "us-east-2") {
                message
            }
        }

        OR

        {
            createBucket(bucketName: "dragon333", region: "us-east-2")
        }



    """
    class Arguments:
        bucket_name = graphene.String()
        region = graphene.String(required=False)

    message = graphene.String()

    def mutate(self, info, bucket_name, region=None):

        if region == None:
            region = settings.AWS_DEFAULT_REGION  # "ap-south-1" for India

        try:
            s3_client = boto3.client('s3', region_name=region)
            location = {'LocationConstraint': region}
            s3_client.create_bucket(Bucket=bucket_name, CreateBucketConfiguration=location)

            message = 'Created bucket {0} in the S3 region ({1})'.format(bucket_name, region)

        except ClientError as e:
            message = e.response["Error"]["Message"]
        except Exception as e:
            message = str(e)

        return BucketMessage(message)


class BucketQuery(graphene.ObjectType):
    buckets = graphene.List(graphene.String)
    exists = graphene.String(name=graphene.String())

    create_bucket = CreateBucket.Field()
    delete_bucket = DeleteBucket.Field()

    def resolve_buckets(self, info):
        """
            GraphiQL Command to check if a bucket exists or not
            {
              buckets
            }
        """

        s3_client = boto3.resource('s3')

        buckets = []

        for bucket in s3_client.buckets.all():
            buckets.append(bucket.name)

        return buckets

    def resolve_exists(self, info, name):
        """
            GraphiQL Command to check if a bucket exists or not
            {
              exists(name:"spg-test")
            }
        """

        message = ''

        bucket_name = name

        try:
            s3_client = boto3.client('s3')
            response = s3_client.head_bucket(Bucket=bucket_name)

            if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
                message = '{0} exists and you have permission to access it.'.format(bucket_name)
            else:
                message = 'Man! Something Else Went Wrong.'

        except ClientError as e:
            message = e.response["Error"]["Message"]
        except Exception as e:
            message = str(e)

        return message


schema = graphene.Schema(query=BucketQuery)


